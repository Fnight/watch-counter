import React, { Component } from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.css";
import { Well, Button, ButtonToolbar, Modal, FormGroup, FormControl, ControlLabel, Alert} from "react-bootstrap";
import firebase from 'firebase';

const config = {
	apiKey: "AIzaSyBNJEAgtlu7LMtsfxNViFt1IPUfbxaCGMs",
	authDomain: "watch-counter.firebaseapp.com",
	databaseURL: "https://watch-counter.firebaseio.com",
	projectId: "watch-counter",
	storageBucket: "watch-counter.appspot.com",
	messagingSenderId: "751158741992"
};
firebase.initializeApp(config);
//firebase.database().ref('/test').push({message: 'I want to test this shit'});

class OneSeries extends Component {
	render() {
		return(
			<Well>
				{this.props.id}
			</Well>
		);
	}
}

class Series extends Component {
	render() {
		return (
			<div className="Series">
				{this.props.series.map((id, index) => (
						<OneSeries key={index} id={id._id}/>
					))}
			</div>
		);
	}
}

class LoginModal extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isShowingLoginModal: true,
			errorCode: null,
			errorMessage: null
		};
		this.login = this.login.bind(this);
		this.register = this.register.bind(this);
		console.log(props);
	}
	componentWillReceiveProps(nextProps) {
		this.setState({isShowingLoginModal : nextProps.id == null ? true : false});
	}
	register() {
		if (this.modalEmail.value && this.modalPassword.value) {
			let here = this;
			firebase.auth().createUserWithEmailAndPassword(this.modalEmail.value, this.modalPassword.value).catch(function(error) {
				here.setState({errorCode: error.code});
				here.setState({errorMessage: error.message});
			});
			firebase.auth().onAuthStateChanged(function(user) {
				if (user) {
					here.setState({ isShowingLoginModal: false});
					here.props.callbackLogin(user.uid);
				}
			});
		}
	}
	login() {
		if (this.modalEmail.value && this.modalPassword.value) {
			let here = this;
			firebase.auth().signInWithEmailAndPassword(this.modalEmail.value, this.modalPassword.value).catch(function(error) {
				here.setState({errorCode: error.code});
				here.setState({errorMessage: error.message});
			});
			firebase.auth().onAuthStateChanged(function(user) {
				if (user) {
					here.setState({ isShowingLoginModal: false});
					here.props.callbackLogin(user.uid);
				}
			});
		}
	}
	render() {
		return(
			<div className="LoginModal">
				<Modal show={this.state.isShowingLoginModal}>
					<Modal.Header>
						<Modal.Title>Login to see you series</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<FormGroup>
							<ControlLabel>Email</ControlLabel>
							<FormControl type="email" placeholder="Enter your email" inputRef={ref => {this.modalEmail = ref;}}/>
						</FormGroup>
						<FormGroup>
							<ControlLabel>Password</ControlLabel>
							<FormControl type="password" placeholder="Enter your login" inputRef={ref => {this.modalPassword = ref;}}/>
						</FormGroup>
						{this.state.errorMessage ?
							<Alert bsStyle="danger">{this.state.errorMessage}</Alert> : null}
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={this.register}>Register</Button>
						<Button onClick={this.login}>Login</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}
}

class AddSeriesModal extends Component {
	constructor() {
		super();
		this.state = {
			isShowingAddModal: false,
		};
		this.addNewSeries = this.addNewSeries.bind(this);
	}
	addNewSeries() {
		let series = this.state.series;
		series.push({
			title: this.addModalTitle.value,
			description: this.addModalDescription.value,
			year: Number(this.addModalYear.value),
			status: this.addModalStatus.value
		});
		this.setState({ series: series, isShowingAddModal:false});
	}
	render() {
		return(
			<Modal show={this.state.isShowingAddModal} onHide={() => this.setState({ isShowingAddModal: false })}>
				<Modal.Header closeButton>
					<Modal.Title>Add new series</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<FormGroup>
						<ControlLabel>Title</ControlLabel>
						<FormControl type="text" placeholder="Enter title" inputRef={ref => {this.addModalTitle = ref;}}/>
					</FormGroup>
					<FormGroup>
						<ControlLabel>Description</ControlLabel>
						<FormControl type="text" placeholder="Enter description (optional)" inputRef={ref => {this.addModalDescription = ref;}}/>
					</FormGroup>
					<FormGroup>
						<ControlLabel>Year</ControlLabel>
						<FormControl type="number" placeholder="Enter year (optional)" inputRef={ref => {this.addModalYear = ref;}}/>
					</FormGroup>
					<FormGroup>
						<ControlLabel>Status</ControlLabel>
						<FormControl componentClass="select" placeholder="Select status" inputRef={ref => {this.addModalStatus = ref;}}>
							<option value="1">Announced</option>
							<option value="2">Ongoing</option>
							<option value="3">Complete</option>
							<option value="4">Close</option>
						</FormControl>
					</FormGroup>
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={this.addNewSeries}>Add series</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}

class AllSeries extends Component {
	constructor() {
		super();
		this.state = {
			series: []
		};
	}
	componentDidMount() {
	}
	render() {
		return(
			<Series series={this.state.series}/>
		);
	}
}

class App extends Component {
	constructor() {
		super();
		this.state = {
			series: [],
			isShowingAddModal: false,
			id: null,
		};
	}
	componentDidMount() {
		let here = this;
		firebase.auth().onAuthStateChanged(function(user) {
			if (user) {
				here.setState({id: user.uid});
			}
		});
	}
	render() {
		return (
			<div className="App">
				<ButtonToolbar>
					<Button bsStyle="primary" onClick={() => this.setState({ isShowingAddModal: true })}>Add new series</Button>
				</ButtonToolbar>
				<AllSeries />
			{/*Модал добавления нового сериала*/}
				<AddSeriesModal />
			{/*Модал логина*/}
				<LoginModal id={this.state.id} callbackLogin={(id) => {this.setState({id: id});}}/>
			</div>
		);
	}
}

export default App;
